<link href="style/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
</script>
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.91.0.min.js"></script>
<script type="text/javascript" src="testMaya.js"></script>

           <div id="main" class="card">
            <!--<div id="header"><div id="header_left"></div>-->
            <!--<div id="header_main">Maya Testing</div><div id="header_right"></div></div>-->
            <div class="card-body">
                <h4>Zeus</h4>
                <div id="content">
                    <form action="upload.php" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="startUpload();">
                         <p id="f1_upload_process" style="padding: 15px 0;">Loading...<br/><img src="loader.gif" /><br/></p>
                         <p id="f1_upload_form" align="center"><br/>
                           <div id="browseFile" onclick="browseFile()">
                              <span id="fileName">Click here to browse file...</span>
                           </div>
                                  <input class="hide" id="fileUploader" name="myfile" type="file" size="30" />
                         </p>
                             <div class="text-center btnContainer" id="uploadMayaLabel">
                                 <button class="btn btn-primary" type="submit"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                           
                             </div>
                        <div class="text-center btnContainer" style ="display:none;" id="textMayaLabel">
                                 <button type="submit" name="submitBtn" class="btn btn-success" value="Upload" onclick="readAndTestQuestions()">Test My BOT</button>
                        </div>
                         
                         <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
                     </form>
                     <div class="text-center" id="downloadFileContainer">
                         <a href="" id="a" style ="display:none;"><button class="btn btn-warning" onclick="download('file,text', 'response.csv', '.csv')"> download file</button></a>
                   
                    </div>
                 </div>
            </div>     
             
             </div>


<script language="javascript" type="text/javascript">

function startUpload(){
	//alert("hello");
      document.getElementById('f1_upload_process').style.visibility = 'visible';
      document.getElementById('f1_upload_form').style.visibility = 'hidden';
      return true;
}

function stopUpload(success){
    console.log("success is:::"+success);
      var result = '';
      if (success == 1){
          $("#textMayaLabel").show();
         result = '<span class="msg">The file was uploaded successfully!<\/span><br/><br/>';
         $("#uploadMayaLabel").hide();
      }
      else {
         result = '<span class="emsg">There was an error during file upload!<\/span><br/><br/>';
      }
      document.getElementById('f1_upload_process').style.visibility = 'hidden';
      
      document.getElementById('f1_upload_form').style.visibility = 'visible';      
      return true;   
}

</script>  
