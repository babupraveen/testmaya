'use strict';
var fs = require('fs');
var AWS = require('aws-sdk');
var botName = "Maya";
var lex_bot_name = 'Maya'; //AbbFAQBot//localStorage.botname;
var lex_bot_alias = 'Maya_PhaseOne'; //abbFAQBot//localStorage.botaliasname;
var responseTxt;
var waitingForLexResponse;

var sessionAttributes = {
  someKey: 'STRING_VALUE',
  employeeId: '123'
  /* anotherKey: ... */
};

AWS.config.update({
  accessKeyId: "AKIAJLCBDXDUFSRJ7WNA",
  secretAccessKey: "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",
  "region": "us-east-1"
});
var lexruntime = new AWS.LexRuntime();
AWS.config.apiVersions = {
  lexruntime: '2016-11-28',
};
var arry = [];
process.argv.forEach((val, index) => {
  arry.push(val);
  // console.log('value is:'+arry[2]);
});

processTestMaya(arry);

function processTestMaya(filename) {
  try {
    var i = 1;
    var data = fs.readFileSync(filename[2], 'utf8');
    var lines = data.split('\n');
    for (var line in lines) {
      testQuestion(lines[line], 'mayaTesing' + i++);
    }

  } catch (e) {
    console.log('Error:', e.stack);
  }
}

function testQuestion(line, userName) {
  if (line === 'Question,Answer') {} else {
    var question = "";
    var answer = "";
    if (line.indexOf(',"') > -1 && line !== 'Question,Answer') {
      var parts = line.split(',"');
      question = parts[0];
      answer = parts[1].slice(0, parts[1].length - 1);

    } else if (line !== 'Question,Answer') {
      var parts = line.split(",");
      question = parts[0];
      answer = parts[1];

    }
    setTimeout(function(wait) {
      if (line === "") {} else {
        postQuestionToLex(question, answer, userName);
      }
    }, 1000, waitingForLexResponse);
    while (waitingForLexResponse) {
      sleep(100);
    }
  }
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    console.log("Dummy");
    if ((new Date().getTime() - start) > milliseconds) {
      break;
    }
  }
}

function postQuestionToLex(question, expectedAnswer, userName) {
  try {
    var params = {
      botAlias: lex_bot_alias,
      /* required */
      botName: lex_bot_name,
      /* required */
      inputText: question,
      /* required */
      userId: '123',
      /* required */
      sessionAttributes: {
        someKey: 'STRING_VALUE',
        employeeId: '123'
        /* anotherKey: ... */
      }
    }
    var gotResponse = false;
    var answer;
    try {
      var resp = lexruntime.postText(params, function(err, data) {
        if (err) console.log("error is:::::::::" + err, err.stack); // an error occurred
        else {
          var answerReceived = data.message;
          var matchScore = compareStrings(expectedAnswer, answerReceived);
          var match = (matchScore > 0.8) ? 'Exact Match' : ((matchScore > 0.5) ? 'Partial Match' : 'Mismatch');
          var matchScore = (matchScore > 0.8) ? matchScore : ((matchScore > 0.5) ? matchScore : matchScore);
          console.log('"' + question + '","' + expectedAnswer + '","' + answerReceived + '","' + match + '"' + matchScore);
          gotResponse = true;
          answer = '"' + question + '","' + expectedAnswer + '","' + answerReceived + '","' + match + '","' + matchScore + '"';
          responseTxt += answer + '\n';
          waitingForLexResponse = false;
        }
      });
    } catch (e) {
      console.log('Exception in Lex response is :)' + e);
    }
    waitingForLexResponse = true;
  } catch (e) {
    console.log('Exception in lex response is :)' + e);
  }
}

function compareStrings(s1, s2) {
  var longer = s1;
  var shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  var longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}

function editDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  var costs = new Array();
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0)
        costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}

function download(text, name, type) {
  text = responseTxt;
  console.log("Writing to file:: File Name: " + name + "\n Content: " + text);
  var a = document.getElementById("a");
  var file = new Blob([text], {
    type: type
  });
  a.href = URL.createObjectURL(file);
  a.download = name;
}
